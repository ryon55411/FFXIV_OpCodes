# FFXIV_OpCodes

## お知らせ
現状は6.3x,6.5xからのみ変換できます。
## 事前準備
事前にPtyhon3を導入してください。入れ方分からない人は[このあたりを見てください](https://qiita.com/Haruka-Ogawa/items/b37d0a2b48d14e29e802)  
次にコマンドプロンプトでRequestsをpipでインストールしてください。
```
pip install requests
```
## 使い方
このリポジトリにある`replay_updater_online.py`をダウンロードをして任意のフォルダーに入れます、合わせて変換したいリプレイファイルも`replay_updater_online.py`と同じフォルダーに入れることをオススメします。  
次にコマンドプロンプトで`replay_updater_online.py`が入っているフォルダに移動します。  
　　Tips. 目的のフォルダをエクスプローラーで開き、アドレスバーに`cmd`を入力してエンターすればコマンドで移動する手間が省けます。  

下記コマンドをコマンドプロンプトで入力してエンターをします。
```
python replay_updater_online.py "変換したいリプレイファイル名.dat"
```
すると  
```
SupportVersion:
 Global_2024.02.05
 Global_2024.01.06
TargetVer(defaultGlobal_2024.02.05):
```
と聞かれますので匿名化及び最新のパッチに対応させるのであればそのままエンターをします。  
しばらくすると`Saved on プレイファイル名_Global_バージョン名.dat`と表示されてフォルダにファイルが保存されます。  
そのファイルを元あったフォルダに移動させれば匿名化及び最新のパッチに対応してコンテンツリプレイを再生できます。

## 参考サイト等
[元のプログラムのリポジトリ](https://github.com/moewcorp/FFXIVNetworkOpcodes)